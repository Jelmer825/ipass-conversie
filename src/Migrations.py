from src.Database import Database

class Migrations:
    def __init__(self):
        self.migrate_all()

    def migrate_all(self):
        """
        Function to migrate all tables.
        :return Null:
        """
        db = Database()
        companies = Companies_Migration()
        recommendations = Recommendations_Migration()

        recommendations.down(db)
        companies.down(db)
        companies.up(db)
        recommendations.up(db)

class Companies_Migration:
    def up(self, db):
        """
        Create companies table
        :param db:
        :return Null:
        """
        query = """CREATE TABLE Companies (
                company_id int NOT NULL AUTO_INCREMENT,
                latitude varchar(255),
                longitude varchar(255),
                state varchar(255),
                county varchar(255),
                city varchar(255),
                district varchar(255),
                street varchar(255),
                industry varchar(255),
                company_size int,
                PRIMARY KEY (company_id)
                );"""
        db.execute_query(query, True)
        print("Migrated table Companies!")

    def down(self, db):
        """
        Drop table Companies
        :param db:
        :return None:
        """
        query = """DROP TABLE IF EXISTS Companies"""
        db.execute_query(query, True)
        print("Dropped table Companies!")

class Recommendations_Migration:
    def up(self, db):
        """
        Create recommendations table
        :param db:
        :return None:
        """
        query = """
        CREATE TABLE Recommendations (
                                recommendation_id int AUTO_INCREMENT NOT NULL,
                                company_one_id int NOT NULL,
                                company_two_id int NOT NULL,
                                weight int NOT NULL,
                                PRIMARY KEY (recommendation_id),
                                CONSTRAINT FK_company_one_id
                                FOREIGN KEY (company_one_id) REFERENCES Companies(company_id) ON DELETE CASCADE,
                                CONSTRAINT FK_company_two_id
                                FOREIGN KEY (company_two_id) REFERENCES Companies(company_id) ON DELETE CASCADE
                                );"""
        db.execute_query(query, True)
        print("Migrated table Recommendations!")

    def down(self, db):
        """
        Drop table recommendations
        :param db:
        :return None:
        """
        query = """DROP TABLE IF EXISTS Recommendations"""
        db.execute_query(query, True)
        print("Dropped table Recommendations!")