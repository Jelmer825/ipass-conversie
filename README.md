# README #

### What is this repository for? ###

* This repository is for the conversion algorithm.
* This algorithm converts data from the live db to the analyse database.

### Setup ###

* Make a database according to the ERD. *
* Create a .env file 
* Install modules 

#### ENV example ####

* DB_LIVE_HOST=
* DB_LIVE_DATABASE=
* DB_LIVE_USERNAME=
* DB_LIVE_PASSWORD=
* 
* DB_ANALYSE_HOST=
* DB_ANALYSE_DATABASE=
* DB_ANALYSE_USERNAME=
* DB_ANALYSE_PASSWORD=

### Install modules

* pip install mysqlclient 2.0.3
* pip install pdoc3 0.9.2
* pip install python-dotenv 0.18.0

### Run tests
python Tests.py

### Run program
python Converter.py