from src.Converter import Converter
import threading

def go():
    """
    Starts program on a period
    :return None:
    """
    convert()
    # Run algorithm every 12 hours.
    threading.Timer(43200, go).start()

def convert():
    """
    Defines converter object and starts converting
    :return None:
    """
    converter = Converter()
    converter.start_convert()

if __name__ == '__main__':
    go()

