import os
from src.Database import Database
import requests

class Converter:
    def start_convert(self):
        self.convert_data()

    def convert_data(self):
        """
        Convert data from live database to analyse database
        :return None:
        """
        # Database object
        db = Database()

        # Insert into companies | REPLACE INTO is to prevent dupes when running on a period
        sql = "REPLACE INTO Companies (company_id, latitude, longitude, state, county, city, district, street, industry, company_size) " \
              "VALUES (%s, %s, %s, %s, %s, %s, %s, %s, %s, %s)"

        # Get data from live database
        tuple_list = self.get_live_data()

        # Insert data in analyse database
        db.execute_many_query(sql, tuple_list, True)
        print("Converted new data!")

    def get_live_data(self):
        """
        Get data from live database
        :return List of tuples:
        """

        # API key for https://www.here.com/
        API_KEY = os.environ.get('API_KEY')

        # Database object
        db = Database()

        # Select all records from companies
        query = """SELECT * FROM companies"""
        data = db.get_query(query, False)

        tuple_list = []

        # Loop through data
        for row in data:
            try:
                # Api request to get more information about the companies location
                response = requests.get(f'https://discover.search.hereapi.com/v1/discover?at={row[5]},{row[6]}&q={row[3]}&apiKey={API_KEY}').json()

                if response.get('items'):
                    if len(response['items']) >= 1:

                        # Check if keys exists, otherwise set to empty string
                        if not response['items'][0]['address'].get('state'):
                            response['items'][0]['address']['state'] = ""

                        if not response['items'][0]['address'].get('county'):
                            response['items'][0]['address']['county'] = ""

                        if not response['items'][0]['address'].get('city'):
                            response['items'][0]['address']['city'] = ""

                        if not response['items'][0]['address'].get('district'):
                            response['items'][0]['address']['district'] = ""

                        if not response['items'][0]['address'].get('street'):
                            response['items'][0]['address']['street'] = ""

                        # Generate tuple for inserting
                        tup = (row[0], row[5], row[6],
                               response['items'][0]['address']['state'],
                               response['items'][0]['address']['county'],
                               response['items'][0]['address']['city'],
                               response['items'][0]['address']['district'],
                               response['items'][0]['address']['street'],
                               row[10], row[11])

                        tuple_list.append(tup)
            except KeyError as e:
                print(f"Key error: {e}")
        return tuple_list
Converter()