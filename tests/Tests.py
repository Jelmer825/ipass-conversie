import unittest
from src.Database import Database
import os
from dotenv import load_dotenv
class TestDatabase(unittest.TestCase):
    def test_connect(self):
        load_dotenv()
        DB_ANALYSE_HOST = os.environ.get('DB_ANALYSE_HOST')
        DB_ANALYSE_DATABASE = os.environ.get('DB_ANALYSE_DATABASE')
        DB_ANALYSE_USERNAME = os.environ.get('DB_ANALYSE_USERNAME')
        DB_ANALYSE_PASSWORD = os.environ.get('DB_ANALYSE_PASSWORD')
        db = Database()

        self.assertTrue(db.connect(DB_ANALYSE_HOST,
                                   DB_ANALYSE_DATABASE,
                                   DB_ANALYSE_USERNAME,
                                   DB_ANALYSE_PASSWORD))
if __name__ == '__main__':
    unittest.main()